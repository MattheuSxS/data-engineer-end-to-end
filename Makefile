.PHONY: deploy_airflow_dev entre_bash destroy_airflow_dev

export _PROJECT_ID=$(_PROJECT_ID)
export REGIAO_ID=$(REGIAO_ID)
export TOPICS_ID=$(TOPICS_ID)

deploy_airflow_dev:
	nerdctl run -d -p 8080:8080 -v "$(PWD)/pipe:/opt/airflow/dags/" \
	--env "_PROJECT_ID=$(_PROJECT_ID)" \
	--env "REGIAO_ID=$(REGIAO_ID)" \
	--env "TOPICS_ID=$(TOPICS_ID)" \
	--env "GOOGLE_APPLICATION_CREDENTIALS="r'/opt/airflow/dags/app/credentials.json'"" \
	--env "LIBRARIES_TO_INSTALL=requests, pytz, python-dotenv" \
	--entrypoint=/bin/bash \
	--name airflow apache/airflow:2.4.3-python3.8 \
	-c '(airflow db init && \
		airflow users create --username admin --password abc123 --firstname Matheus --lastname Lastname --role Admin --email mattheusxs@gmail.com
		); \
	airflow webserver & \
	airflow scheduler \
	'

entre_bash:
	nerdctl exec -it airflow bash

destroy_airflow_dev:
	nerdctl rm -f airflow
