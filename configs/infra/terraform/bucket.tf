resource "google_storage_bucket" "cloud_storage_name_dataflow" {
  name                        = var.cloud_storage_name_dataflow
  project                     = var.project
  location                    = var.region
  force_destroy               = true
  uniform_bucket_level_access = true

  lifecycle_rule {
    condition {
      age = 3
    }
    action {
      type = "Delete"
    }
  }

  lifecycle_rule {
    condition {
      num_newer_versions = 3
    }
    action {
      type = "Delete"
    }
  }

  lifecycle_rule {
    condition {
      age = 1
    }
    action {
      type = "AbortIncompleteMultipartUpload"
    }
  }

}

resource "google_storage_bucket_object" "dataflow_template" {
  depends_on = [google_storage_bucket.cloud_storage_name_dataflow]

  name          = "template/${var.cloud_dataflow_name}"
  bucket        = google_storage_bucket.cloud_storage_name_dataflow.name
  source        = "./template/${var.cloud_dataflow_name}"
  storage_class = var.stotage_class_standard
  metadata = {
    content-type = "application/octet-stream"
  }
}

resource "google_storage_bucket_object" "my_dag" {
  depends_on = [google_composer_environment.airflow_mts_default_project]

  name   = "dags/pipeline-trigger-dev.py"
  bucket = replace(replace(google_composer_environment.airflow_mts_default_project.config[0].dag_gcs_prefix, "gs://", ""), "/dags", "")
  source = "../../../pipe/pipeline-trigger-dev.py"
  metadata = {
    content-type = "text/x-python-script"
  }
}

resource "google_storage_bucket_object" "main_files" {
  depends_on = [google_composer_environment.airflow_mts_default_project]

  name   = "dags/src/main.py"
  bucket = replace(replace(google_composer_environment.airflow_mts_default_project.config[0].dag_gcs_prefix, "gs://", ""), "/dags", "")
  source = "../../../src/main.py"
  metadata = {
    content-type = "text/x-python-script"
  }
}

resource "google_storage_bucket_object" "modulo_files" {
  depends_on = [google_composer_environment.airflow_mts_default_project]

  name   = "dags/src/pub_sub.py"
  bucket = replace(replace(google_composer_environment.airflow_mts_default_project.config[0].dag_gcs_prefix, "gs://", ""), "/dags", "")
  source = "../../../src/pub_sub.py"
  metadata = {
    content-type = "text/x-python-script"
  }
}
