resource "google_project_iam_member" "composer_extension_role" {
  project = var.project
  role    = "roles/composer.worker"
  member  = "serviceAccount:${var.cloud_service_account_composer}"
}

resource "google_project_iam_member" "composer_extension_role_v1" {
  project = var.project
  role    = "roles/composer.ServiceAgentV2Ext"
  member  = "serviceAccount:${var.cloud_service_account_composer}"
}
