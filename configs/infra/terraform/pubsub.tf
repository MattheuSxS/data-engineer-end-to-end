resource "google_pubsub_topic" "de_challenge" {
  project = var.project
  name    = var.cloud_pub_sub_topic_name

  labels = {
    created_by = "terraform"
    env        = "dev"
  }

  message_retention_duration = "604800s"
}


resource "google_pubsub_subscription" "sub" {
  depends_on = [google_pubsub_topic.de_challenge]
  project    = var.project
  name       = var.cloud_pubsub_name
  topic      = google_pubsub_topic.de_challenge.name

  labels = {
    created_by = "terraform"
    env        = "dev"
  }

  message_retention_duration = "1200s"
  retain_acked_messages      = true

  ack_deadline_seconds = 20

  expiration_policy {
    ttl = "300000.5s"
  }
  retry_policy {
    minimum_backoff = "60s"
  }

  enable_message_ordering = false
}
