terraform {
    required_providers {
        google = {
            source = "hashicorp/google"
            version = "5.0.0"
            }
        }
    backend "gcs" {
        bucket = "bkt-mts-default-project-terraform"
        prefix = "de_end_to_end/state"
    }
}

provider "google" {
    project = var.project
    region  = var.region
}
