#   ****************************************************    #
#                         Cloud Project                     #
#   ****************************************************    #
variable "project" {
    type    = string
    default = "mts-default-project"
}

variable "region" {
    type    = string
    default = "us-east1"
}

variable "zone" {
    type    = string
    default = "us-east1-b"
}

#   ****************************************************    #
#                           BigQuery                        #
#   ****************************************************    #
variable "bigquery_dataset_raw" {
    type    = string
    default = "de_challenge"
}

variable "bigquery_table_raw" {
    type    = string
    default = "randomuser"
}

variable "schema_tb_randomuser" {
  default = "./schemas/tb_mts_data_engineer.json"
}

#   ****************************************************    #
#                         cloud Composer                    #
#   ****************************************************    #
variable "cloud_composer_name" {
    type    = string
    default = "airflow-mts-default-project"
}

variable "cloud_service_account_composer" {
    type    = string
    default = "service-1082073204188@cloudcomposer-accounts.iam.gserviceaccount.com"
}

#   ****************************************************    #
#                         Cloud  Bucker                     #
#   ****************************************************    #
variable "cloud_storage_name_dataflow" {
    type    = string
    default = "bkt-dfl-tb-randomuser"
}

variable "stotage_class_standard" {
    type    = string
    default = "STANDARD"
}

#   ****************************************************    #
#                         Cloud Pub/Sub                     #
#   ****************************************************    #
variable "cloud_pub_sub_topic_name" {
    type    = string
    default = "mts-data-engineer-challenge"
}

variable "cloud_pubsub_name" {
    type    = string
    default = "mts-de-challenge-sub"
}

#   ****************************************************    #
#                       Service Account                     #
#   ****************************************************    #
variable "cloud_service_account" {
    type    = string
    default = "1082073204188-compute@developer.gserviceaccount.com"
}

#   ****************************************************    #
#                         Dataflow                          #
#   ****************************************************    #
variable "cloud_dataflow_name" {
    type    = string
    default = "dfl-randomuser-nrt"
}
