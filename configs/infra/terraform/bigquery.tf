resource "google_bigquery_dataset" "bigquery_dataset_raw" {
  dataset_id                 = var.bigquery_dataset_raw
  friendly_name              = var.bigquery_dataset_raw
  description                = "Analytical dataset - test tecnico data engineer"
  project                    = var.project
  location                   = var.region
  delete_contents_on_destroy = false

  labels = {
    env = "dev"
  }
}

resource "google_bigquery_table" "bigquery_table_raw" {
  project             = var.project
  dataset_id          = var.bigquery_dataset_raw
  table_id            = var.bigquery_table_raw
  deletion_protection = false
  depends_on          = [google_bigquery_dataset.bigquery_dataset_raw]

  time_partitioning {
    type  = "DAY"
    field = "publish_time"
  }

  labels = {
    "created_by" : "terraform",
    "layer" : "raw"
  }

  clustering = [
    "country",
    "city",
    "gender",
  ]

  schema = file(var.schema_tb_randomuser)

}
