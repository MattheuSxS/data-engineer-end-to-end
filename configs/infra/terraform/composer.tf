resource "google_composer_environment" "airflow_mts_default_project" {
  depends_on = [
    google_project_iam_member.composer_extension_role,
    google_project_iam_member.composer_extension_role_v1
  ]
  project = var.project
  name    = var.cloud_composer_name
  region  = var.region

  config {

    node_count = 3

    node_config {
      disk_size_gb = 50
      zone         = var.zone
      machine_type = "n1-standard-1"
    }

    software_config {
      image_version  = "composer-1.20.12-airflow-2.4.3"
      python_version = "3"
      pypi_packages = {
        requests      = "==2.31.0",
        python-dotenv = "==1.0.0",
        pytz          = "==2023.3.post1"
      }
      env_variables = {
        _PROJECT_ID      = "mts-default-project"
        REGIAO_ID        = "us-east1"
        DATASET_ID       = "de_challenge"
        TABLE_ID         = "randomuser"
        TOPICS_ID        = "mts-data-engineer-challenge"
        SUBSCRIPTIONS_ID = "projects/mts-default-project/subscriptions/mts-de-challenge-sub"
      }
    }

    database_config {
      machine_type = "db-n1-standard-2"
    }

    web_server_config {
      machine_type = "composer-n1-webserver-2"
    }
  }

  lifecycle {
    prevent_destroy = false
  }
}
