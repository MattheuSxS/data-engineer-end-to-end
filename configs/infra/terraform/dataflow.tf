
resource "google_dataflow_job" "streaming_job" {
  depends_on = [
    google_storage_bucket.cloud_storage_name_dataflow
  ]
  project                 = var.project
  region                  = var.region
  name                    = var.cloud_dataflow_name
  template_gcs_path       = "gs://${var.cloud_storage_name_dataflow}/template/${var.cloud_dataflow_name}"
  temp_gcs_location       = "gs://${var.cloud_storage_name_dataflow}/temp"
  enable_streaming_engine = true

  max_workers  = 3
  machine_type = "n1-standard-1"
  on_delete    = "cancel"
}
