import logging
from src.main import main
from airflow.decorators import dag, task
from airflow.utils.dates import days_ago
from datetime import timedelta


logging.basicConfig(
    format=("%(asctime)s | %(levelname)s | File_name ~> %(module)s.py "
            "| Function ~> %(funcName)s | Line ~> %(lineno)d  ~~>  %(message)s"),
    level=logging.DEBUG
    )


default_args = {
    'owner': 'Dev_Matheus',
    'start_date':days_ago(1),
    'retries': 3,
    'retry_delay': timedelta(minutes=15),
    'description': "This DAG triggers the pipeline job",
    'project_id': 'Data_Engineering',
    'tags': [
        "Project: data-engineer-challange",
        "layer: raw",
    ]
}

@dag(dag_id='de_challange_dev', default_args=default_args, schedule_interval='0 7 * * *')


def data_engineer_challange():
    '''
        Google Cloud Platform information
    '''

    @task()
    def begin(): pass

    @task()
    def end(): pass

    @task()
    def call_streaming():
        main()


    begin() >> call_streaming() >> end()


data_engineer_challange()