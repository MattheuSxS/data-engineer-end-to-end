"""
Gets the random user API data and writes the data to a Kafka topic every 10 seconds
"""
import os
import time
import pytz
import logging
import requests

from datetime import datetime
from dotenv import load_dotenv
from os.path import join, dirname
from src.pub_sub import send_message_to_pubsub


dotenv_path = join(dirname(__file__), 'env.sh')
load_dotenv(dotenv_path)


logging.basicConfig(
    format=\
        (
            "%(asctime)s | %(levelname)s | File_name ~> %(module)s.py "
            "| Function ~> %(funcName)s | Line ~> %(lineno)d  ~~>  %(message)s"
        ),
    level=logging.INFO
)

PROJECT_ID          = os.getenv('_PROJECT_ID')
DATASET_ID          = os.getenv('DATASET_ID')
TABLE_ID            = os.getenv('TABLE_ID')
TOPICS_ID           = os.getenv('TOPICS_ID')
SUBSCRIPTIONS_ID    = os.getenv('SUBSCRIPTIONS_ID')


def create_response_dict(url: str="https://randomuser.me/api/?results=1") -> dict:
    """
    Creates the results JSON from the random user API call
    """
    response = requests.get(url)
    data = response.json()

    return data["results"][0]


def create_final_json(data_dict: dict) -> dict:
    """
    Creates the final JSON to be sent to Kafka topic only with necessary keys
    """

    utc_now = datetime.now(pytz.utc)
    sao_paulo_tz = pytz.timezone('America/Sao_Paulo')
    sp_now = utc_now.astimezone(sao_paulo_tz)

    kafka_data = dict(
        message_id      = sp_now.strftime('%Y%m%d%H%M%S'),
        publish_time    = sp_now.strftime('%Y-%m-%d %H:%M:%S'),
        data_dict       = data_dict
    )
    return kafka_data


def main():
    """
    Writes the API data every 10 seconds to Kafka topic random_names
    """

    end_time = time.time() + 30
    while True:
        if time.time() > end_time:
            break

        data_dict = create_response_dict()
        pub_sub_data = create_final_json(data_dict=data_dict)
        result = send_message_to_pubsub(
            mensagem_dict=pub_sub_data,
            project_id=PROJECT_ID,
            topic_id=TOPICS_ID
        )

        logging.info(result)
        time.sleep(5)


if __name__ == "__main__":
    main()
