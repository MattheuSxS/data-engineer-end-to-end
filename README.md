# 🚧 IN PROGRESING 🚧

# Data Engineer End-to-End
First of all, please visit my repo to be able to understand the whole process better. This project will illustrate a streaming data pipeline and also includes many modern Data tech stack. I also want to mention that I used MacOS for this project.


## Tech Stack
    Python
    API
    Apache Airflow  ( GCP ~ Composer )
    Apache Kafka    ( GCP ~ Pub/Sub )
    Apache Beam     ( GCP ~ Dataflow)
    BigQuery
    Cloud Storage
    IAM
    Docker


## Introduction
We will use Random Name API to get the data. It generates new random data every time we trigger the API. We will get the data using our first Python script. We will run this script regularly to illustrate the streaming data. This script will also write the API data to the Pub/Sub topic. We will also schedule and orchestrate this process using the Airflow DAG script. Once the data is written to the Pub/Sub producer, we can get the data via Spark Beam Streaming script. Then, we will write the modified data to BigQuery using the same script. All the services will be running Google Cloud Plataform ( GCP ). You can test local too.


To complete the challenge, Python 3.11.5 was used. To run the code locally, you need to download the project by using 'Git clone' and install the necessary dependencies for code execution. All the commands below are executed in the **CMD** in the **project's root folder**. I recommend the user to use one of the options below as a tool for creating the environment:

- Anaconda/Miniconda:

        conda create –n $(basename $(pwd)) python=3.11.6
        conda activate $(basename $(pwd))

- Pyenv

        python3.11 -m venv app_env
        source app_env/bin/activate


To install the necessary dependencies to run the code, use the following command:

    pip install -r requirements-dev.txt
    or
    pip3 install -r requirements-dev.txt

It takes a few minutes to complete the entire installation, and the environment is ready for use. Sometimes, when a virtual environment is created, Python may not recognize the directory and display some strange errors, so it is recommended to use the following command in the project's root folder:

    export PYTHONPATH=$(pwd)


## Testes Automatizados
O projeto tem sua pasta de testes localizada em;

- scr/test
	- tests

- O usuário executa esses testes entrando em um dos dois diretórios citado acima e executando um dos comandos abaixo;
    - make test
    - make coverage


## Automated Tests
The project has its test folder located in:

- src/test
    - tests

- The user runs these tests by entering one of the two directories mentioned above and executing one of the following commands:
    - make test
    - make coverage

![alt text](./docs/data-engineer-challange.png)


## Code structure
```
.
├── data-engineer-end-to-end
│   ├── configs
│   │   ├── docker
│   │   └── infra
│   │       └── terraform
│   │           ├── schemas
│   │           │   └── tb_mts_data_engineer.json
│   │           ├── template
│   │           │   └── dfl-randomuser-nrt
│   │           ├── bigquery.tf
│   │           ├── bucket.tf
│   │           ├── composer.tf
│   │           ├── dataflow.tf
│   │           ├── main.tf
│   │           ├── pubsub.tf
│   │           ├── service_account.tf
│   │           └── variable.tf
│   ├── docs
│   │   ├── README.md
│   │   └── data-engineer-challange.png
│   ├── pipe
│   │   ├── src
│   │   │   ├── __init__.py
│   │   │   ├── credentials.json
│   │   │   ├── main.py
│   │   │   └── pub_sub.py
│   │   ├── pipeline-trigger-dev.py
│   │   └── pipeline-trigger-prd.py
│   ├── src
│   │   ├── tests
│   │   │   ├── test_dfl_randomuser_nrt.py
│   │   │   ├── test_pub_sub.py
│   │   │   └── test_main.py
│   │   ├── .coveragerc
│   │   ├── dfl_randomuser_nrt.py
│   │   ├── env.sh
│   │   ├── main.py
│   │   ├── Makefile
│   │   ├── pub_sub.py
│   │   ├── requirements-dev.txt
│   │   ├── requirements.txt
│   │   └── setup.py
├── .gitignore
├── .gitlab-ci.yml
├── .python-version
├── Makefile
└── README.md
```


## References

- GitLab:
    - [Document GitLab](https://docs.gitlab.com)
    - [Command GitLab Clone](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)
    - [GitLab CI/CD](https://docs.gitlab.com/ee/ci/)

- Terraform:
    - [Document Terraform](https://developer.hashicorp.com/terraform/intro)
    - [Terraform Google](https://registry.terraform.io/providers/hashicorp/google/latest)

- Python:
    - [Document Python](https://docs.python.org/3.11/)
    - [Document Pytest](https://docs.pytest.org/en/7.4.x/contents.html)
    - [Document Coverage](https://coverage.readthedocs.io/en/7.3.2/)

- Apache Bem
    - [Document Apache Beam](https://beam.apache.org/documentation/sdks/python/)

- Airflow:
    - [Document Airflow](https://airflow.apache.org/docs/apache-airflow/2.4.3/index.html)
    - [Scheduler Airflow](https://airflow.apache.org/docs/apache-airflow/2.4.3/concepts/scheduler.html)

- Google Cloud Platform (GCP):
    - [Document Composer](https://cloud.google.com/composer/docs/run-apache-airflow-dag)
    - [Document Cloud Storage](https://cloud.google.com/storage/docs?hl=pt-br)
    - [Document DataFlow](https://cloud.google.com/dataflow/docs?hl=pt-br)
    - [Document Pub/Sub](https://cloud.google.com/pubsub/docs?hl=pt-br)
    - [Document IAM Service Account](https://cloud.google.com/iam/docs/service-account-overview?hl=pt-br)

- Docker:
    - [Document Docker](https://docs.docker.com)
    - [Docker Airflow](https://hub.docker.com/r/apache/airflow)

- Makefile
    - [Document Makefile](https://www.gnu.org/software/make/manual/make.html)
    - [Document Tutorial](https://makefiletutorial.com)


| Versão Do Documento |         Editor          |    Data    |
|        :---:        |         :---:           |    :---:   |
|        2.0.0        |   Matheus Dos S. Silva  | 30/10/2023 |