import json
import argparse
import apache_beam as beam
from apache_beam.io.gcp.pubsub import ReadFromPubSub
from apache_beam.io.gcp.bigquery import WriteToBigQuery
from apache_beam.options.pipeline_options import PipelineOptions


def build_output_payload(payload:dict) -> dict:
    """ Load json payload containing structure to be used in writing the table

    Args:
        message dict: receive a dictionary uploaded from the pub/sub thread

    Returns:
        list[dict]: returns a list of dictionaries
        example output:
        [
            {
                message_id: 12324345
                cod_evento: 2023-04-30 21:19:19.297000 UTC
                .... : ....
                .... : ....
            },
            {....},
            {....}
        ]
    """

    output_payload = dict(
        message_id      = payload['message_id'],
        publish_time    = payload['publish_time'],
        full_name       = f"{payload['data_dict']['name']['title']}. {payload['data_dict']['name']['first']} {payload['data_dict']['name']['last']}",
        gender          = payload['data_dict']["gender"],
        location        = f"{payload['data_dict']['location']['street']['number']}, {payload['data_dict']['location']['street']['name']}",
        city            = payload['data_dict']['location']['city'],
        country         = payload['data_dict']['location']['country'],
        postcode        = payload['data_dict']['location']['postcode'],
        latitude        = float(payload['data_dict']['location']['coordinates']['latitude']),
        longitude       = float(payload['data_dict']['location']['coordinates']['longitude']),
        email           = payload['data_dict']["email"]
    )

    return output_payload


def get_schema_bigquery(project:str, dataset:str, table:str) -> str:
    """ This function retrieves the structure of an existing BigQuery
    table and returns that structure. This returned schema can then be used
    to ensure data conformance during the insert operation, ensuring that the
    inserted data matches the table's predefined schema."

    Args:
        project:str = Project Name
        dataset:str = Dataset Name
        table:str = Table Name

    Returns:
        str: returns a string with the field name and its type
        example output:
            dt_hr_evento:TIMESTAMP, cod_evento:INTEGER, cod_origem_evento:STRING, ....
    """
    from google.cloud import bigquery
    client = bigquery.Client(project=project)
    table_ref = client.dataset(dataset).table(table)
    _table = client.get_table(table_ref)

    field_types = [f"{field.name}:{field.field_type}" for field in _table.schema]
    str_schema = ", ".join(field_types)

    return str_schema


def pipeline_run(_runner:str, project_id:str, project_dataflow_id:str, subscription_id:str, bkt_stage:str, bkt_temp:str, bkt_template:str) -> None:

    PROJECT_ID      = project_id
    DATASET_ID      = 'de_challenge'
    TABLE_ID        = 'randomuser'
    SUBSCRIPTION_ID = subscription_id

    pipeline_options = dict(
        runner              = _runner,
        project             = project_dataflow_id,
        region              = 'us-east1',
        job_name            = 'dfl-randomuser-nrt',
        num_workers         = 1,
        max_num_workers     = 3,
        worker_machine_type = 'n1-standard-1',
        staging_location    = bkt_stage,
        temp_location       = bkt_temp,
        template_location   = bkt_template,
        streaming           = True,
        )

    options     = PipelineOptions.from_dictionary(pipeline_options)
    pipeline    = beam.Pipeline(options=options)

    messages = (pipeline
                | 'Read from Pub/Sub' >> ReadFromPubSub(subscription=SUBSCRIPTION_ID)
                | 'Decode messages' >> beam.Map(lambda x: x.decode('utf-8'))
                )

    deserialized_messages = (messages
                            | 'Deserialize messages' >> beam.Map(lambda x: json.loads(x))
                            | 'Process Pub/Sub Messages' >> beam.Map(build_output_payload)
                            )

    deserialized_messages | 'Write to BigQuery' >> WriteToBigQuery(
        table=f'{PROJECT_ID}.{DATASET_ID}.{TABLE_ID}',
        schema=get_schema_bigquery(project=PROJECT_ID, dataset=DATASET_ID, table=TABLE_ID),
        create_disposition=beam.io.BigQueryDisposition.CREATE_IF_NEEDED,
        write_disposition=beam.io.BigQueryDisposition.WRITE_APPEND,
        method='STREAMING_INSERTS'
       )

    pipeline.run()#.wait_until_finish()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--runner",
        type=str,
        choices=['DataflowRunner', 'DirectRunner'],
        required=False,
        default='DataflowRunner',
        help="Choose where apache-beam will run!"
    )
    parser.add_argument(
        "--project",
        type=str,
        required=False,
        default='mts-default-project',
        help="What project is the table in"
    )
    parser.add_argument(
        "--project_dataflow",
        type=str,
        required=False,
        default='mts-default-project',
        help="Choose which project apache-beam will run!"
    )
    parser.add_argument(
        "--subscription",
        type=str,
        required=False,
        default='projects/mts-default-project/subscriptions/mts-de-challenge-sub',
        help="Choose which subscription apache-beam will used!"
    )
    parser.add_argument(
        "--bkt_stage",
        type=str,
        required=False,
        default='gs://bkt-dfl-tb-randomuser/staging',
        help="In which bucket were the Staging files!"
    )
    parser.add_argument(
        "--bkt_temp",
        type=str,
        required=False,
        default='gs://bkt-dfl-tb-randomuser/temp',
        help="In which bucket were the Temp files!"
    )

    parser.add_argument(
        "--bkt_template",
        type=str,
        required=False,
        default='gs://bkt-dfl-tb-randomuser/template/dfl-randomuser-nrt',
        help="In which bucket were the Temp files!"
    )

    args = parser.parse_args()

    pipeline_run(
        _runner             = args.runner,
        project_dataflow_id = args.project_dataflow,
        project_id          = args.project,
        subscription_id     = args.subscription,
        bkt_stage           = args.bkt_stage,
        bkt_temp            = args.bkt_temp,
        bkt_template        = args.bkt_template
    )


# python dfl-randomuser-nrt.py --runner "DataflowRunner" --project "mts-default-project" --project_dataflow "mts-default-project" --subscription "projects/mts-default-project/subscriptions/mts-de-challenge-sub" --bkt_stage "gs://bkt-dfl-tb-randomuser/staging" --bkt_temp "gs://bkt-dfl-tb-randomuser/temp"
# python dfl-randomuser-nrt.py --runner "DirectRunner" --project "mts-default-project" --project_dataflow "mts-default-project" --subscription "projects/mts-default-project/subscriptions/mts-de-challenge-sub" --bkt_stage "gs://bkt-dfl-tb-randomuser/staging" --bkt_temp "gs://bkt-dfl-tb-randomuser/temp"