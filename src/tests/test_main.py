import unittest
from unittest.mock import patch
from src.main import *
from dotenv import load_dotenv
from os.path import join, dirname


dotenv_path = join(dirname(__file__), 'env.sh')
load_dotenv(dotenv_path)


class TestMain(unittest.TestCase):

    @patch('src.main.requests.get')
    def test_create_response_dict(self, mock_requests_get):
        # Defina os dados de exemplo que você espera da API
        mock_api_response = {
            "results": [
                {
                    "name": {"first": "John", "last": "Doe"},
                    "email": "john.doe@example.com",
                }
            ]
        }

        # Configure o retorno da função mock de requests.get
        mock_requests_get.return_value.json.return_value = mock_api_response

        # Chame a função que você deseja testar
        result = create_response_dict()

        # Verifique se a função retornou os dados esperados
        expected_result = {
            "name": {"first": "John", "last": "Doe"},
            "email": "john.doe@example.com",
        }

        self.assertEqual(result, expected_result)


    def test_create_final_json(self):
        # Defina um dicionário de exemplo para data_dict
        data_dict = {
            "field1": "value1",
            "field2": "value2",
            # Adicione outros campos conforme necessário
        }

        # Chame a função que você deseja testar
        result = create_final_json(data_dict)

        # Obtenha o tempo atual em São Paulo (America/Sao_Paulo)
        sao_paulo_tz = pytz.timezone('America/Sao_Paulo')
        sp_now = datetime.now(sao_paulo_tz)

        # Crie um dicionário esperado com as chaves e valores esperados
        expected_result = {
            "message_id": sp_now.strftime('%Y%m%d%H%M%S'),
            "publish_time": sp_now.strftime('%Y-%m-%d %H:%M:%S'),
            "data_dict": data_dict
        }

        # Verifique se o resultado da função corresponde ao esperado
        self.assertEqual(result, expected_result)
