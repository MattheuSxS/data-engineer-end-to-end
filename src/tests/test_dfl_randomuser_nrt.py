import os
import unittest
from pathlib import Path
from os.path import join
from dotenv import load_dotenv
from unittest.mock import MagicMock
from apache_beam.transforms.util import BatchElements
from src.dfl_randomuser_nrt import *
from apache_beam.testing.test_pipeline import TestPipeline
from apache_beam.testing.util import assert_that, equal_to


dotenv_path = join(Path(__file__).parent.parent, 'env.sh')
load_dotenv(dotenv_path)


class TestBuildDataflow(unittest.TestCase):

    def test_build_output_payload(self):
        # Dicionário de entrada simulado
        input_payload = {
            "message_id": "12345",
            "publish_time": "2023-04-30 21:19:19.297000 UTC",
            "data_dict": {
                "name": {
                    "title": "Mr",
                    "first": "John",
                    "last": "Doe"
                },
                "gender": "Male",
                "location": {
                    "street": {
                        "number": 123,
                        "name": "Main St"
                    },
                    "city": "New York",
                    "country": "USA",
                    "postcode": "10001",
                    "coordinates": {
                        "latitude": "40.7128",
                        "longitude": "-74.0060"
                    }
                },
                "email": "johndoe@example.com"
            }
        }

        # Chame a função para obter a saída
        output = build_output_payload(input_payload)

        # Dicionário de saída esperado
        expected_output = {
            "message_id": "12345",
            "publish_time": "2023-04-30 21:19:19.297000 UTC",
            "full_name": "Mr. John Doe",
            "gender": "Male",
            "location": "123, Main St",
            "city": "New York",
            "country": "USA",
            "postcode": "10001",
            "latitude": 40.7128,
            "longitude": -74.006,
            "email": "johndoe@example.com"
        }

        # Compare a saída com o resultado esperado
        self.assertEqual(output, expected_output)
