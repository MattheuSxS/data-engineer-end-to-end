import os
import unittest
from dotenv import load_dotenv
from pathlib import Path
from os.path import join
from unittest.mock import Mock, patch
from src.pub_sub import send_message_to_pubsub


dotenv_path = join(Path(__file__).parent.parent, 'env.sh')
load_dotenv(dotenv_path)


class TestSendMessageToPubsub(unittest.TestCase):

    @patch('google.cloud.pubsub_v1.PublisherClient')
    def test_send_message_to_pubsub(self, mock_publisher_client):
        # Define os valores de teste
        mensagem_dict = {'message': 'Hello, Pub/Sub!'}
        project_id = os.getenv('_PROJECT_ID')
        topic_id = os.getenv('DATASET_ID')

        # Crie uma instância de Mock para o publisher e configure o comportamento esperado
        mock_publisher = Mock()
        mock_publisher_client.return_value = mock_publisher

        # Simule o retorno de topic_path
        mock_publisher.topic_path.return_value = f'projects/{project_id}/topics/{topic_id}'

        mock_publisher.publish.return_value.result.return_value = '1'  # Simulando um ID de mensagem

        # Chame a função que você deseja testar
        result = send_message_to_pubsub(mensagem_dict, project_id, topic_id)

        # Verifique se a função de publish foi chamada com os argumentos corretos
        mock_publisher.publish.assert_called_once_with(f'projects/{project_id}/topics/{topic_id}', data=b'{"message": "Hello, Pub/Sub!"}',)

        # Verifique se o resultado da função corresponde ao esperado
        self.assertEqual(result, f'Message published in topic projects/{project_id}/topics/{topic_id}: 1')
