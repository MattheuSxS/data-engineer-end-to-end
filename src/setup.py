import version
from setuptools import setup, find_packages


setup(
    name='Data Engineer End to End',
    version=version.VERSION,

    description='Data Engineer End-to End Challenge',

    author='Matheus Dos S. Silva',
    author_email='mattheusxs@gmail.com',

    packages=find_packages(exclude=['venv', 'dist', 'docs', 'tests']),
    py_modules=[
        'pub_sub',
    ],
    install_requires=[
        'requests==2.31.0',
        'pytz==2023.3.post1',
        'python-dotenv==1.0.0',
        'apache-beam[gcp]==2.51.0',
        'google-cloud-pubsub==2.18.4',
        'google-cloud-bigquery==3.12.0',
        'Flask',
        'pytest',
        'coverage',
        'pytest-cov',
        'pytest-html',
        'mock',
        'pylint',
        'autopep8',
        'unidecode'
    ],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Operating System :: POSIX',
        'Programming Language :: Python :: 3.11.5',
        'Topic :: Scientific/Engineering :: Information Analysis'
    ],
    entry_points='''
        [console_scripts]
        spb_etl=main:cli
    '''
)
