import json
import logging
from google.cloud import pubsub_v1


logging.basicConfig(
    format=\
        (
            "%(asctime)s | %(levelname)s | File_name ~> %(module)s.py "
            "| Function ~> %(funcName)s | Line ~> %(lineno)d  ~~>  %(message)s"
        ),
    level=logging.INFO
)


def send_message_to_pubsub(mensagem_dict: dict, project_id: str, topic_id: str) -> str:
    """
    Sends the message to Pub/Sub topic
    """
    publisher = pubsub_v1.PublisherClient()

    topic_path = publisher.topic_path(project_id, topic_id)
    future = publisher.publish(topic_path, data=json.dumps(mensagem_dict).encode('utf-8'))
    result = future.result()


    return f'Message published in topic {topic_path}: {result}'
